"use strict";

document.addEventListener("DOMContentLoaded", function(){
    /** UI*/

    //= ui/animation.js
    //= ui/chart.js
    //= ui/docsListsToggle.js
    //= ui/dropdown.js
    //= ui/form-select.js
    //= ui/input-datepicker.js
    //= ui/lightbox.js
    //= ui/modal.js
    //= ui/project-tabs.js
    //= ui/regInvisibleBlock.js
    //= ui/scrollbars.js
    //= ui/sidebar-navigation.js
    //= ui/sliders.js
    //= ui/switcherToggle.js
    //= ui/tabs.js
    //= ui/team-list.js
    //= ui/textarea.js
    //= ui/toggle.js
});