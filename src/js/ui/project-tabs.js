const   projectTabs = document.querySelectorAll('.project__tabs .tab'),
        projectTabBlocks = document.querySelectorAll('.project__tabs .tabs__block'),
        parentProjectTabs = document.querySelector('.project__tabs');

function hideProjectsTabs(){
    projectTabBlocks.forEach(item => {
        item.classList.remove('show', 'fade');
        item.classList.add('hide');
    });

    projectTabs.forEach(item => {
        item.parentElement.classList.remove('active');
    });
}

function showProjectsTabs(i = 0){
    projectTabBlocks[i].classList.remove('hide');
    projectTabBlocks[i].classList.add('show', 'fade');

    projectTabs[i].parentElement.classList.add('active');
}

if(projectTabs.length > 0){
    hideProjectsTabs();
    showProjectsTabs();

    parentProjectTabs.addEventListener('click', e => {
        const target = e.target;

        if(target && target.classList.contains('tab')){
            e.preventDefault();

            projectTabs.forEach((item, i) => {
                if(target == item){
                    hideProjectsTabs();
                    showProjectsTabs(i);
                }
            });
        }
    });
}