const scrollbars = document.querySelectorAll('.scrollbar');

scrollbars.forEach(item => {
    const ps = new PerfectScrollbar(item, {
        wheelSpeed: 1,
        wheelPropagation: false,
    });
});