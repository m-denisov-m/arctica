const toggles = document.querySelectorAll('.js-toggle');

toggles.forEach(item => {
    const data = item.getAttribute('data-item');
    
    item.addEventListener('click', (e) => {
        const   target = e.target,
                block = document.querySelector(`.toggle__block[data-item=${data}]`);

        slideToggle(block);
        if(target == item){
            item.classList.toggle('active');
        }else{
            target.parentElement.classList.toggle('active');
        }
    });
});

