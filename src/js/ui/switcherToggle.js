const switchers = document.querySelectorAll('.switcher__toggle');

switchers.forEach(item => {
    item.addEventListener('click', () => {
        item.classList.toggle('active');

        const   data = item.getAttribute('data-switcher'),
                blocks = document.querySelectorAll(`.switcher__block[data-switcher=${data}]`);

        blocks.forEach(block => {
            if(block.classList.contains('hide')){
                block.classList.remove('hide');
            }else{
                block.classList.add('hide');
            }
        });

        if(item.hasAttribute('data-switcher-tab')){
            const labels = item.parentElement.querySelectorAll('.switcher__labels');
            labels.forEach(label => {
                if(label.classList.contains('active')){
                    label.classList.remove('active');
                }else{
                    label.classList.add('active');
                }
            });
        }
    });
});